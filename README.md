#Code Challenge

Given input files listing individual campaign contributions and percentile value, `donation-analytics.py` identifies repeat donors and total dollars received, total number of contributions received, and donation amount in given percentile, and writes the information in `repeat_donors.txt`. This program assumes that the the first input given is a file that contains the list of campaign contributions, the second input is the percentile file (without mistakes), and the third input is the file to output information to.

There is a `run.sh` file to compile and run this program. This solution is written in Python 2.7 using the standard libraries.