#!/usr/bin/env python

#Author: Eileen Cho

from collections import OrderedDict, defaultdict
from operator import itemgetter
import re
import sys
from datetime import datetime

def stream(filename, percentile, outname):
  inFile = open(filename,"r")
  percentFile = open(percentile,"r")
  wantedPercentile = float(percentFile.read())/100
  outFile = open(outname,"w")
  allDonations = {} #store valid donations to check
  recipients = {} #format of each item: {(recipient,zip,year):[contributionCount,append sorted contributions]}
  infoIndex = OrderedDict()
  infoIndex["CMTE_ID"] = 1
  infoIndex["NAME"] = 8
  infoIndex["ZIP_CODE"]= 11
  infoIndex["TRANSACTION_DT"] = 14
  infoIndex["TRANSACTION_AMT"] = 15
  infoIndex["OTHER_ID"] = 16

  #stream input
  for line in inFile:
    newDonation = OrderedDict()
    rawData = line.split("|")

    #create donation info
    for i in infoIndex:
      newDonation[i] = rawData[infoIndex[i]-1]

    #validate donation
    if not flagged(newDonation):
      #create IDs to search donations and recipients
      newDonation["ZIP_CODE"] = newDonation["ZIP_CODE"][:5]
      newDonation["TRANSACTION_DT"] = newDonation["TRANSACTION_DT"][-4:]
      donorID = (newDonation["NAME"],newDonation["ZIP_CODE"])
      recipientID = (newDonation["CMTE_ID"],newDonation["ZIP_CODE"],newDonation["TRANSACTION_DT"])
      
      #check if repeat donor exists yet in allDonations
      #if yes, append to found value, calculate info and emit
      if donorID in allDonations:
        allDonations[donorID].append(newDonation)
        #increase donor counter (first list item) and add contribution amount to list
        if recipientID in recipients:
          recValue = recipients[recipientID]
          contrCounter = recValue[0] + 1 
          moneyList = recValue[1:]
          moneyList.append(float(newDonation["TRANSACTION_AMT"]))
          moneyList.sort()
          recValue = [contrCounter] + moneyList
          recipients[recipientID] = recValue
        else: #first repeat donor found for recipientID
          recipients[recipientID] = [1,float(newDonation["TRANSACTION_AMT"])]
        if latestYear(allDonations[donorID]):
          donationPercentile, total = calc(recipients[recipientID],wantedPercentile)
          toEmit = [newDonation["CMTE_ID"],newDonation["ZIP_CODE"],newDonation["TRANSACTION_DT"],str(donationPercentile),str(total),str(recipients[recipientID][0])]
          outFile.write("|".join(toEmit) + "\n")
      #if not a repeat donor, create new key for later lookup
      else:
        allDonations[donorID] = [newDonation]
      
  inFile.close()
  outFile.close()

#true means ignore donation, don't use for consideration
#false means not flagged, keep this donation
def flagged(donation):
  indiv = bool(donation["OTHER_ID"]) #empty if individual
  anyEmpty = bool('' in donation.values()[:-1]) #checks if any required fields are empty
  dtCheck = len(donation["TRANSACTION_DT"])!=8 and not donation["TRANSACTION_DT"].isdigit()
  try:
    datetime.strptime(donation["TRANSACTION_DT"],'%m%d%Y').date()
  except:
    dtCheck = True
  zipCheck = len(donation["ZIP_CODE"])<5 and not donation["ZIP_CODE"].isdigit()
  #nameCheck = #if malformed then record will never be found, so essentially ignored

  return indiv or anyEmpty or dtCheck or zipCheck

def latestYear(donorInfo):
  allyears = [d["TRANSACTION_DT"] for d in donorInfo]
  latest = max(enumerate(allyears),key=itemgetter(1))[0]
  return latest == len(allyears)-1

#calculate percentile ranking
def calc(recipient, wantedPercentile):
  contributions = len(recipient)-1
  total = int(sum(recipient[1:]))
  if contributions == 1:
    rank = 1
  else:
    rank = int(round(wantedPercentile * contributions))
  donationPercentile = int(round(recipient[rank]))
  return donationPercentile, total

def main():
  stream(sys.argv[1],sys.argv[2],sys.argv[3])

if __name__ == "__main__":
  main()
